package com.example.spring.mvc.rest.SpringBootEmpMvcEx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEmpMvcExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEmpMvcExApplication.class, args);
	}

}
