'use strict';

App.controller('EmployeeController', ['$scope', 'User', function($scope, User) {
          var self = this;
          self.employee= new Employee();
          
          self.users=[];
              
          self.fetchAllEmployees = function(){
        	  self.employees = Employee.query();
          };
           
          self.createEmployee = function(){
        	  self.employee.$save(function(){
        		  self.fetchAllmployee();
        	  });
          };
		  
          self.updateEmployee = function(){
        	  self.user.$update(function(){
    			  self.fetchAllEmployees();
    		  });
          };

         self.deleteEmployee = function(identity){
        	 var employee = Employee.get({id:identity}, function() {
        		  employee.$delete(function(){
        			  console.log('Deleting user with id ', identity);
        			  self.fetchAllEmployees();
        		  });
        	 });
          };

          self.fetchAllEmployees();

          self.submit = function() {
              if(self.employee.id==null){
                  console.log('Saving New employee', self.employee);    
                  self.createEmployee();
              }else{
    			  console.log('Upddating employee with id ', self.employee.id);
                  self.updateEmployee();
                  console.log('User updated with id ', self.employee.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.users.length; i++){
                  if(self.users[i].id === id) {
                     self.user = angular.copy(self.users[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              if(self.user.id === id) {//If it is the one shown on screen, reset screen
                 self.reset();
              }
              self.deleteUser(id);
          };

          
          self.reset = function(){
              self.user= new User();
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
