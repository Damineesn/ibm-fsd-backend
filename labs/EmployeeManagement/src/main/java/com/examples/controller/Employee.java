package com.examples.controller;

import java.io.Serializable;

public class Employee implements Serializable {


	private static final long serialVersionUID = 1L;

	

	private String empNo;

	private String firstName;

	private String lastName;

	private String deptName;



	public String getEmpNo() {

		return empNo;

	}

	public void setEmpNo(String empNo) {

		this.empNo = empNo;

	}

	public String getFirstName() {

		return firstName;

	}


	public void setFirstName(String firstName) {

		this.firstName = firstName;

	}


	public String getLastName() {

		return lastName;

	}

	public void setLastName(String lastName) {

		this.lastName = lastName;

	}


	public String getDeptName() {

		return deptName;

	}

	public void setDeptName(String deptName) {

		this.deptName = deptName;

	}

	@Override

	public String toString() {

		return "Employee [empNo=" + empNo + ", firstName=" + firstName + ", lastName=" + lastName + ", deptName="

				+ deptName + "]";

	}

	

	

}