package com.examples.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import com.examples.controller.*;



@Service

public class EmployeeServiceImpl  {



	@Autowired

	public EmployeeDaoImpl employeeDao;

	

	public List<Employee> getListOfEmployee() {

		return employeeDao.getListOfEmployee();

	}



	public boolean addEmployee(Employee employee) {

		return employeeDao.addEmployee(employee);

	}



	public boolean updateEmployee(Employee employee) {

		return employeeDao.updateEmployee(employee);

	}



	public boolean deleteEmployee(String empNo) {

		return employeeDao.deleteEmployee(empNo);

	}



}
