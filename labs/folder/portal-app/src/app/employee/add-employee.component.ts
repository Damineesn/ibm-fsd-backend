import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { employee } from '../models/employee.model';
import { EmployeeService } from './employee.service';

@Component({
  templateUrl: './add-employee.component.html'
})
export class AddEmployeeComponent {

  Employee: employee = new Employee();

  constructor(private router: Router, private EmployeeService: EmployeeService) {

  }

  createEmployee(): void {
    this.EmployeeService.createEmployee(this.employee)
        .subscribe( data => {
          alert("Employee created successfully.");
        });

  };

}
