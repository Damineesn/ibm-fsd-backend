import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { employee } from '../models/employee.model';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styles: []
})
export class EmployeeComponent implements OnInit {

  employees: employee[];

  constructor(private router: Router, private EmployeeService: employeeService) {

  }

  ngOnInit() {
    this.EmployeeService.getemployees()
      .subscribe( data => {
        this.employees = data;
      });
  };

  deleteEmployee(Employee: employee): void {
    this.EmployeeService.deleteEmployee(employee)
      .subscribe( data => {
        this.employees = this.employees.filter(u => u !== employee);
      })
  };

}


