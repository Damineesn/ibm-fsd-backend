package com.examples.java.servlet;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet{
	
	
	public void init()
	{
		System.out.println("hi");
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("get method invoked");
		this.doPost(request, response);
	}
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("Contextpath:"+request.getContextPath());
		Enumeration<String> ab=request.getHeaderNames();
		while(ab.hasMoreElements())
		{
			String headername=ab.nextElement();
			System.out.println("headername"+response.getHeaders(headername));
		}
		response.setContentType("text/html");
			 PrintWriter out = response.getWriter();
			 out.println("hello world");
			out.println("<html>");
			out.println("<body>");
			out.println("<h1>Daminee<h1>");
			out.println("<body>");
			out.println("<html>");
			
		
	}
	public void destroy()
	{
		System.out.println("destroyed");
	}
}
