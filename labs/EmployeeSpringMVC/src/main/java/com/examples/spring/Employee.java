package com.examples.spring;

public class Employee {
	private int id;
	 private String name;
	 private int salary;
	 private int age;
	 private  String department;
	private String message;

		public Employee( int id,String name,int age,  String department,int salary) {
			this.id=id;
			this.name=name;
			this.salary=salary;
			this.age=age;
			this.department=department;
			
		}
		public int getId()
		{
			return id;
		}
		public void setEmpId(int id) {
			this.id = id;
		}
		public String getName()
		{
			return name;
		}
		public void setEmpName(String name) {
			this.name = name;
		}
		public int getAge()
		{
			return age;
		}
		public void setAge(int age)
		{
			this.age=age;
		}
		public int getSalary()
		{
			return salary;
		}
		public void setSalary(int salary)
		{
			this.salary=salary;
		}
		public String getDept()
		{
			return department;
		}
		public void setDept(String department)
		{
			this.department=department;
		}
		public Employee() {
		
		}
		public void setMessage(String string) {
			this.message=string;
			
		}
		public void add(Employee employee) {
			this.age=employee.getAge();
			this.id=employee.getId();
			this.salary=employee.getSalary();
			this.name=employee.getName();
			this.department=employee.getDept();
		}
	
	
}
