package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class ReflectionEx {
	
	public static void main(String[] args)
	{
		try
		{
			Class<?> c=calculator.class;
			System.out.println("class is");System.out.println(c);
			Constructor<?> constr=c.getConstructor();
			System.out.println("the constructor");System.out.println(constr);
			
			Object obj=constr.newInstance();
			System.out.println("the obj is");
			//System.out.println(obj);
			Method add1=c.getMethod("add",Integer.class,Integer.class);
			int m=(int) add1.invoke(obj,23,24);
			System.out.println(m);
			
			
		}
		catch(Exception e)
		{
			System.out.println("exception");
		}
	}
	

}
