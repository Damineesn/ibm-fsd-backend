package com.examples.spring.web.mvc.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@ModelAttribute("user")
	public Map<String,User> userDetails()
	{
		Map<String, User>user = new TreeMap<String, User>();
		user.put("Daminee","abcv" );
		user.put("Chandana","abkj");
		return user;
	}
	@ModelAttribute("Mobilenumber")
	public int number()
	{
		
	}
	
}
