package com.examples.spring.web.mvc.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.examples.spring.web.mvc.model.User;

public class UserService {

private static Map<String, User> users = new LinkedHashMap<String, User>();
	

	public void add(User user)
	{
		users.put(user.getName(), user);
	}
	
	public void update(User user)
	{
		
		users.put(user.getName(), user);
	}
	
	public User get(String name)
	{
		return users.get(name);
	}
	
	public void delete(String name)
	{
		users.remove(name);
	}	
	
	public List<User> list()
	{
		return new ArrayList<User>(users.values());
	}	
	
}


