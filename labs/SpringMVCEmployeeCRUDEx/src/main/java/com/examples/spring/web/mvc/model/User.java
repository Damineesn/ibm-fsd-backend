package com.examples.spring.web.mvc.model;

public class User {
	private String name;
	private String password;
	private int mobile;
	private String mail;
	public User(String name, String password, int mobile, String mail) {
		
		this.name = name;
		this.password = password;
		this.mobile = mobile;
		this.mail = mail;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getMobile() {
		return mobile;
	}
	public void setMobile(int mobile) {
		this.mobile = mobile;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	

}
