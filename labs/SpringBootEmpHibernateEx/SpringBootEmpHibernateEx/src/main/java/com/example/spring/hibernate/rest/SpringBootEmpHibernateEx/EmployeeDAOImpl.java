package com.example.spring.hibernate.rest.SpringBootEmpHibernateEx;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceProperty;
import javax.persistence.PersistenceUnit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Repository
@Transactional
public class EmployeeDAOImpl implements EmployeeDAO{
	
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	
    @PersistenceContext
	private  EntityManagerFactory entityManagerFactory;

	

	public void addEmployee(Employee emp) {
	      
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.persist(emp);
		entityManager.getTransaction().commit();
		logger.info("Employee saved successfully, Employee Details=" + emp.getId());
		entityManager.close();
	}

	public void updateEmployee(Employee emp1) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		// Fetch order detail for update
		 Employee emp = (Employee) entityManager.find(Employee.class,emp1);
		
		
		// Persist updated order detail
		entityManager.persist(emp);
		entityManager.getTransaction().commit();
		logger.info("Employee updated successfully, Employee Details= " + emp);
	}

	@SuppressWarnings("unchecked")
	public List<Employee> listEmployees() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Employee> employeesList = entityManager.createQuery("select o from Employee o").getResultList();
		for(Employee emp : employeesList){
			logger.info("Employee List::"+ emp);
		}
		return employeesList;
	}

	public Employee getEmployeeById(int id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Employee emp = (Employee)entityManager.find(Employee.class, new Integer(id));
		logger.info("Employee loaded successfully, Employee details=" + emp);
		return emp;
	}

	public void removeEmployee(int id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();		
		// Fetch order detail for update
		Employee emp = (Employee) entityManager.find(Employee.class, id);
		
		// Delete order detail
		entityManager.remove(emp);
		
		entityManager.getTransaction().commit();
	
		logger.info("Employee deleted successfully, Employee details="+ emp);
	}

	


}
