package com.example.spring.hibernate.rest.SpringBootEmpHibernateEx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEmpHibernateExApplication  {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEmpHibernateExApplication.class, args);
	}


}
