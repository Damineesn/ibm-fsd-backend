package com.examples.spring.web.rest.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEmpMgmtServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEmpMgmtServiceApplication.class, args);
	}
   
}
