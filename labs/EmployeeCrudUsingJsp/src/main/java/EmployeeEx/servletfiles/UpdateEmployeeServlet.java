package EmployeeEx.servletfiles;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;



import EmployeeEx.javafiles.EmployeeDAO;



/**

 * Servlet implementation class UpdateEmployeeServlet

 */

public class UpdateEmployeeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

       

   EmployeeDAO emp=new EmployeeDAO();

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  this.doPost(request, response);

   }

   

	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id=request.getParameter("id");

		String name=request.getParameter("name");

		String salary=request.getParameter("salary");

		String age=request.getParameter("age");

		String department=request.getParameter("department");

		response.sendRedirect("form.jsp");

		

		emp.updateX(Integer.parseInt(id),name,Integer.parseInt(salary),department,Integer.parseInt(age));

		

	}



}