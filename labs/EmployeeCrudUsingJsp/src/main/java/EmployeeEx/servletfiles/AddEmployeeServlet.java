package EmployeeEx.servletfiles;




import java.io.IOException;

import java.time.LocalDate;

import java.time.Period;



import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;



import EmployeeEx.javafiles.Employee;
import EmployeeEx.javafiles.EmployeeDAO;
import EmployeeEx.javafiles.EmployeeService;





public class AddEmployeeServlet extends HttpServlet {

	EmployeeDAO emp=new EmployeeDAO();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.doPost(request, response);

	}



	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id=request.getParameter("id");

		String name=request.getParameter("name");

		String salary=request.getParameter("salary");
		
		String department=request.getParameter("department");

		String age=request.getParameter("age");

		emp.insert(name,Integer.parseInt(age),department,Integer.parseInt(salary));

     response.sendRedirect("form.jsp");

	}



}