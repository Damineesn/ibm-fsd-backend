package EmployeeEx.servletfiles;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import EmployeeEx.javafiles.EmployeeDAO;


public class DeleteEmployeeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		EmployeeDAO emp=new EmployeeDAO();

		// HttpSession ses=request.getSession();

		//String did=(String) ses.getAttribute("did");

		String id=request.getParameter("id");

		System.out.println(id);

		

		emp.deleteX(Integer.parseInt(id));

		PrintWriter out=response.getWriter();

		out.println("employee deleted successfully");

		response.sendRedirect("form.jsp");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
	}
}