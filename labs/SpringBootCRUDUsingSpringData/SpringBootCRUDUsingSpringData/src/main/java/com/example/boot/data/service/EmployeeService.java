package com.example.boot.data.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.boot.data.model.Employee;
import com.example.boot.data.repo.EmployeeRepo;

@Service
public class EmployeeService {

//	private static Map<Integer, Employee> employees = new LinkedHashMap<Integer, Employee>();

	@Autowired
	private EmployeeRepo employeeRepo;

	public void add(Employee employee) {
//		int empId = employees.size() + 1;	
//		employee.setId(empId);			
//		employees.put(employee.getId(), employee);
		employeeRepo.save(employee);
	}

	public void update(Employee employee) {

//		employees.put(employee.getId(), employee);
		employeeRepo.save(employee);
	}

	public Employee get(int empId) {
		return employeeRepo.findById(empId).get();
	}

	public void delete(int empId) {
		employeeRepo.deleteById(empId);;
	}

	public List<Employee> list() {		
//		return new ArrayList<Employee>(employees.values());
		return  (List<Employee>) employeeRepo.findAll();
	}

}
