package com.example.boot.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCrudUsingSpringDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCrudUsingSpringDataApplication.class, args);
	}

}
