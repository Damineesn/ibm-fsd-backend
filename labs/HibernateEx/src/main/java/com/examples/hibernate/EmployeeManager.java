package com.examples.hibernate;


import java.util.Iterator;

import java.util.List;



import org.hibernate.HibernateException;

import org.hibernate.Session;

import org.hibernate.SessionFactory;

import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;



import com.examples.hibernate.Employee;
public class EmployeeManager {
	private static SessionFactory sessionFactory ;
	
	public static void main(String[] args)
	{
		Configuration config=new Configuration();
		config.configure();
		
	sessionFactory=config.buildSessionFactory();
		System.out.println(sessionFactory);
		
		Session session=sessionFactory.openSession();
		Transaction tnx=session.beginTransaction();
		
		
		Employee emp=new Employee();
		
		emp.setName("Daminee");
		emp.setAge(24);
		emp.setDepartment("Technical");
		emp.setSalary(23000);
		
		
         listEmployees();

		Integer empid = createEmployee(emp);

		listEmployees();

		updateEmployee(empid, "Admin");

		listEmployees();

		deleteEmployee(empid);

		listEmployees();
		
	}
	private static Integer createEmployee(Employee emp) {



		Transaction tnx = null;

		Integer empid = -1;



		try (Session session = sessionFactory.openSession()) {

			tnx = session.beginTransaction();



			// Insert data into table by supplying the persistent object

			empid = (Integer) session.save(emp);

			

			System.out.println("\nEmployee inserted successfully with ID - " + empid);

			

			tnx.commit();

		} catch (HibernateException he) {

			tnx.rollback();

			he.printStackTrace();

		}

		return empid;

	}
	private static void listEmployees() {

		Transaction tnx = null;



		try (Session session = sessionFactory.openSession()) {

			tnx = session.beginTransaction();



			// List Employee Details

			List<Employee> emps = session.createQuery("FROM Employee").list();



			System.out.println("ID \tName \tAge  \tDepartment \tsalary");

			for (Iterator<Employee> iterator = emps.iterator(); iterator.hasNext();) {

				Employee emp = (Employee) iterator.next();



				System.out.println(emp.getEmpid() + "\t" + emp.getName() + "\t" + emp.getAge() + "\t"

						 + "\t" + emp.getDepartment() + "\t" + emp.getSalary());

			}



			tnx.commit();



		} catch (HibernateException he) {

			tnx.rollback();

			he.printStackTrace();

		}

	}
	private static void updateEmployee(Integer empid, String department) {

		Transaction tnx = null;



		try (Session session = sessionFactory.openSession()) {

			tnx = session.beginTransaction();



			// Update Employee Details

			Employee empForUpdate = session.get(Employee.class, empid);

			empForUpdate.setDepartment(department);

			session.update(empForUpdate);

			System.out.format("\nEmployee %s updated successfuly.\n", empid);

			tnx.commit();



		} catch (HibernateException he) {

			tnx.rollback();

			he.printStackTrace();

		}

	}

	private static void deleteEmployee(Integer empid) {

		Transaction tnx = null;



		try (Session session = sessionFactory.openSession()) {

			tnx = session.beginTransaction();



			// Update Employee Details

			Employee empForDelete = session.get(Employee.class, empid);

			session.delete(empForDelete);



			System.out.format("\nEmployee %s deleted successfuly.\n", empid);



			tnx.commit();



		} catch (HibernateException he) {

			tnx.rollback();

			he.printStackTrace();

		}

	}

}
