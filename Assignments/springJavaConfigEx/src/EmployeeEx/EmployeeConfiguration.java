package EmployeeEx;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.datetime.joda.LocalDateParser;

@Configuration
public class EmployeeConfiguration {
  
	@Bean
	public Employee employee()
	{
		return new Employee(dob(),address(),skills());
	}
	public List<String> skills() {
		 ArrayList<String> abc=new ArrayList<>();
			abc.add("ASD");
		return abc;
	}
	@Bean
	public LocalDate dob() {
		return LocalDate.parse("1996-07-07");
	}
	@Bean
	public address address() {
		return new address("Bangalore","India",560030);
	}

}
