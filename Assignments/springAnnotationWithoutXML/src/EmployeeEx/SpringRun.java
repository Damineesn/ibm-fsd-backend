package EmployeeEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringRun {

	public static void main(String[] args) {
	AbstractApplicationContext context = new AnnotationConfigApplicationContext(EmployeeConfiguration.class);


	employee obj = (employee) context.getBean("employee");

	System.out.println();
	//System.out.println( obj.getDob());
//	System.out.println( obj.getDestination());
//	 System.out.println(obj.getSkills());
	 System.out.println(obj. getAddress().getCity()+"\n "+obj.getAddress().getCountry()+"\n"+obj.getAddress().getPin());
 
	  context.registerShutdownHook();

	
	}

}
