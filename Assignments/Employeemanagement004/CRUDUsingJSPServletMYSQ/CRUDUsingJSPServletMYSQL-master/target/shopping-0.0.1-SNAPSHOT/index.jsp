<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Add new contact</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">

		<c:choose>
			<c:when test="${param.update == 'true'}">
				<h2>Update Contact Information</h2>
				<form method="post" action="update-employee" class="form-inline">
					<c:forEach items="${EmployeeById}" var="val">
						<div class="form-group">
							<label for="id">Id:</label> <input type="id"
								class="form-control" id="id" name="id"
								value="${val.value.id}">
						</div>
						<div class="form-group">
							<label for="name">Name:</label> <input type="name"
								class="form-control" id="name" name="name"
								value="${val.value.name}">
						</div>
						<div class="form-group">
							<label for="age">Age:</label> <input type="age"
								class="form-control" id="age" name="age"
								value="${val.value.age}">
						</div>
						<div class="form-group">
							<label for="department">Department:</label> <input type="department"
								class="form-control" id="department" name="department"
								value="${val.value.department}">
						</div>
						<div class="form-group">
							<label for="salary">Salary:</label> <input type="salary"
								class="form-control" id="salary" name="salary"
								value="${val.value.salary}">
						</div>
						<button type="submit" class="btn btn-default" value="Update">Update</button>
					</c:forEach>
				</form>

			</c:when>
			<c:otherwise>
				<h2>Add new Contact Information</h2>
				<form method="post" action="add-employee" class="form-inline">
				     <div class="form-group">
						<label for="id">Id:</label> <input type="id"
							class="form-control" id="id" name="id"
							placeholder="Enter Id">
					</div>
					<div class="form-group">
						<label for="name">Name:</label> <input type="name"
							class="form-control" id="name" name="name"
							placeholder="Enter name">
					</div>
					<div class="form-group">
						<label for="age">Age:</label> <input type="age"
							class="form-control" id="age" name="age"
							placeholder="Enter age">
					</div>
					<div class="form-group">
						<label for="department">department:</label> <input type="department"
							class="form-control" id="department" name="department"
							placeholder="Enter department">
					</div>
					<div class="form-group">
						<label for="salary">Salary:</label> <input type="salary"
							class="form-control" id="salary" name="salary"
							placeholder="Enter salary">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</c:otherwise>
		</c:choose>




		<table class="table table-striped">
			<thead>
				<tr>
				    <th>Id</th>
					<th>Name</th>
					<th>Age</th>
					<th>Department</th>
					<th>Salary</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${contactList}" var="val">
					<tr>
					<td><c:out value="${val.value.id}" /></td>
						<td><c:out value="${val.value.name}" /></td>
						<td><c:out value="${val.value.age}" /></td>
						<td><c:out value="${val.value.department}" /></td>
						<td><c:out value="${val.value.salary}" /></td>
						<td><a href="update-employee?id=${val.key}"
							class="btn btn-info" role="button">Update</a></td>
						<td><a href="delete-employee?id=${val.key}"
							class="btn btn-info" role="button">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	
</body>
</html>