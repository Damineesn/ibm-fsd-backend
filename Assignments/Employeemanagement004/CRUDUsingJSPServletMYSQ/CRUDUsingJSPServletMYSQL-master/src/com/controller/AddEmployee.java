package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.EmployeeService;


@WebServlet("/add-employee")
public class AddEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String department = request.getParameter("department");
		String salary=request.getParameter("salary");
		EmployeeService employeeService = new EmployeeService();
		employeeService.addEmployee(id,name, age,department,salary);
		HttpSession session = request.getSession();
		session.setAttribute("contactList", employeeService.getAllEmployees());
		response.sendRedirect("index.jsp");

	}

}
