package com.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dao.JDBCConnection;
import com.domain.Employee;

public class EmployeeService {
	private JDBCConnection jdbcConnection;
	private Connection conn = null;
	private PreparedStatement pstmt = null;

	public EmployeeService() {
		jdbcConnection = new JDBCConnection();
	}

	public  void addEmployee(String id,String name, String age , String department,String salary) {
		try {
			Employee employee = new Employee(id ,name, age, department,salary);
			conn = jdbcConnection.getConnection();
			String sql = "INSERT INTO employeedetails(id,name,age,department,salary) VALUES (?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, employee.getId());
			pstmt.setString(2, employee.getName());
			pstmt.setString(3, employee.getAge());
			pstmt.setString(4, employee.getDepartment());
			pstmt.setString(5, employee.getSalary());
			System.out.println(employee);
			System.out.println("sql =" + sql);
			pstmt.executeUpdate();
			System.out.println("Inserted records into the table...");

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

	}

	public Map<Integer, Employee> getAllEmployees() {
		try {
			Map<Integer, Employee> map = new HashMap<>();
			conn = jdbcConnection.getConnection();
			String query = "SELECT * FROM employeedetails";
			pstmt = conn.prepareStatement(query); // create a statement
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee(rs.getString(1),rs.getString(2), rs.getString(3), rs.getString(4),rs.getString(5));
				map.put(rs.getInt(1), employee);
			}
			System.out.println(map);
			return map;
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return null;
	}

	public Map<Integer, Employee> getEmployeeById(String id) {
		try {
			Map<Integer, Employee> mapWithId = new HashMap<>();
			conn = jdbcConnection.getConnection();
			String query = "SELECT * FROM employeedetails WHERE id = ?";
			pstmt = conn.prepareStatement(query); // create a statement
			pstmt.setInt(1,Integer.parseInt(id));
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee(rs.getString(1),rs.getString(2),rs.getString(3), rs.getString(4), rs.getString(5));
				mapWithId.put(Integer.parseInt(id), employee);
			}
			return mapWithId;

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return null;
	}

	// delete records based on id
	public void deleteEmployee(String id) {
		try {
			conn = jdbcConnection.getConnection();
			String query = "DELETE FROM Employeedetails WHERE id = ?";
			pstmt = conn.prepareStatement(query); // create a statement
			pstmt.setInt(1, Integer.parseInt(id));
			// execute delete SQL stetement
			pstmt.executeUpdate();
			System.out.println("Record is deleted!");
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

	}

	// delete records based on id
	public void updateEmployee(String id, String name,String age, String department, String salary) {
		try {
			conn = jdbcConnection.getConnection();
			String query = "UPDATE employee SET  id=?,name = ?,age = ?, department = ?,salary=?" + " WHERE id = ?";
			pstmt = conn.prepareStatement(query); // create a statement
			
			pstmt.setString(1, name);
			pstmt.setString(2, age);
			pstmt.setString(3, department);
			pstmt.setString(4, salary);
			pstmt.setString(4, id);
		
			pstmt.executeUpdate();
			System.out.println("Record is Updated!");
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

	}

}
