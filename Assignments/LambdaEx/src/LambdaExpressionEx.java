
import java.util.function.BiFunction;
import java.util.function.Consumer;

public class LambdaExpressionEx {

	   public static void main(String args[]) {

		   LambdaExpressionEx lamExp = new LambdaExpressionEx();

	      //with type declaration

	      BiFunction<Integer,Integer,Integer> addition = (Integer a, Integer b) -> a + b;

	      //with out type declaration

	      BiFunction<Integer,Integer,Integer> subtraction = (a, b) -> a - b;

			

	      //with return statement along with curly braces

	      BiFunction<Integer,Integer,Integer> multiplication = (Integer a, Integer b) -> { return a * b; };

			

	      //without return statement and without curly braces

	      BiFunction<Integer,Integer,Integer> division = (Integer a,Integer  b) -> a / b;

			

	      System.out.println("10 + 5 = " + lamExp.operate(10, 5, addition));

	      System.out.println("10 - 5 = " + lamExp.operate(10, 5, subtraction));

	      System.out.println("10 x 5 = " + lamExp.operate(10, 5, multiplication));

	      System.out.println("10 / 5 = " + lamExp.operate(10, 5, division));

	      

	   }

	      private int operate(int a, int b,BiFunction<Integer,Integer,Integer> mathOperation) {

		      return mathOperation.apply(a, b);


	   }



	}
