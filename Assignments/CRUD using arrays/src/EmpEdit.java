

public class EmpEdit {
	
	
    private int empNum;
    private double empSal;
    EmpEdit(int empNum, double empSal){
        this.empNum =empNum ;
        this.empSal = empSal;
    }
    public void setEmpNum(int empNum) {
    	this.empNum=empNum;
    }
    public void setEmpSal(double empSal) {
    	this.empSal=empSal;
    }


    public int getEmpNum(){
        return empNum;
    }
    public double getEmpSal(){
        return empSal;
    }
    public void details() {
    	System.out.println("employ num:"+this.getEmpNum());
    	System.out.println("employ sal:"+this.getEmpSal());
    }
    
    public void update(int empNum,double empSal) {
    	this.empNum=empNum;
    	this.empSal=empSal;
    }
    public void delete() {
    	this.empNum=0;
    	this.empSal=0;
    	System.out.println("deleted sucessfully");
    }
  

	public static void main(String[] args) {
		int n=3;
		 EmpEdit[] e = new EmpEdit[n];
		e[0]=new EmpEdit(10,15000.0);
		e[1]=new EmpEdit(11,20000.0);
		e[2]=new EmpEdit(12,25000.0);
		
		
		e[1].delete();
		
		//delete the object
		for(int i=0;i<3;i++)
		{if(e[i].empNum==0 && e[i].empSal==0.0)
		{
			for(int j=i+1;j<3;j++)
			{
			e[j].empNum=e[j].empNum-1;
			}
		}
		else
		{
			e[i].details();
		} 
		}
		
		
		//update the object
		for(int i=0;i<3;i++)
		{
			if(e[i].empNum==10)
			{
				e[i].update(15,30000);
			}
		}
		System.out.println("\n");
		System.out.println("update succesfully");
		
		//Printing the objects
		for(int i=0;i<3;i++)
		{
			if(e[i].empNum==0 && e[i].empSal==0.0)
			{
				continue;
			}
			else {
			e[i].details();
			}
		}                                                                                                                         
		
	}
	
	
	//adding the object
	
   

}
