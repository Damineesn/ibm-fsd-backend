package EmployeeEx;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringRun {

	public static void main(String[] args) {
	AbstractApplicationContext context = new ClassPathXmlApplicationContext("beans-config.xml");


	Employee obj = (Employee) context.getBean("employee");

	System.out.println();
	System.out.println( obj.getDob());
	System.out.println( obj.getDestination());
	 System.out.println(obj.getSkills());
	 System.out.println(obj. getAddress().getCity()+" "+obj.getAddress().getCountry()+" "+obj.getAddress().getPin());
//		System.out.println( obj1.getCountry());
//		System.out.println( obj1.getPin());
	 
	  context.registerShutdownHook();

	
	}

}
