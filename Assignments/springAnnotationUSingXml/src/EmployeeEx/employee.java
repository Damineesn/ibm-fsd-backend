package EmployeeEx;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class employee {
	 int id;
	String name;
	LocalDate dob;
	address addresses;
	 String destination;
	 public address getAddress() {
		
		return addresses;
	}
	public void setAddress(address addresses) {
		this.addresses = addresses;
	}
	double salary;
	 List<String> skills;
	
	
	boolean fulltime;
	public double getSalary() {
		return salary;
	}
	public List<String> getSkills() {
		return skills;
	}
	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public employee(int id, String name, address addresses) {
		this.id = id;
		this.name = name;
		this.addresses = addresses;
	}
	@Autowired(required=true)
	public employee(address addresses) {
	
		this.addresses = addresses;
	}
	public int getId()
	{
		return id;
	}
	public String getName()
	{
		return name;
	}
	
	
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	
	public boolean isFulltime() {
		return fulltime;
	}
	public void setFulltime(boolean fulltime) {
		this.fulltime = fulltime;
	}
	public void print()
	{
		System.out.println();
	}
	@PostConstruct
	public void intiaze()
	{
		System.out.println("Bean is initialized");
	}
	@PreDestroy
	public void cleanup() {
		System.out.println("cleaned");
	}
}

