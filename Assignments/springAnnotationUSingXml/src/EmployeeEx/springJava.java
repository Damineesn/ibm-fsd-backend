package EmployeeEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class springJava {

	public static void main(String[] args) {
	AbstractApplicationContext context = new ClassPathXmlApplicationContext("beans-annotation-config.xml");


	employee obj = (employee) context.getBean("employee");

	System.out.println();
	//System.out.println( obj.getDob());
//	System.out.println( obj.getDestination());
//	 System.out.println(obj.getSkills());
	 System.out.println(obj. getAddress().getCity()+"\n "+obj.getAddress().getCountry()+"\n"+obj.getAddress().getPin());
 
	  context.registerShutdownHook();

	
	}

}
