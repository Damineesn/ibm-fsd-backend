

public class Employ<T extends Number>{
   private int id;
   private int age;
   private String dept;
   private T salary;
   public Employ()
   {
   	System.out.println("Employ default constructor");
   }
   public Employ(int id,int age,String dept,T salary){
   this.id=id;
   this.age=age;
   	this.dept=dept;
   	this.salary=salary;
   }
    public void setId(int id) {
    	this.id=id;
    }
    public void setAge(int age) {
    	this.age=age;
    }
    public void setDept(String dept) {
    	this.dept=dept;
    }
    public void setSalary(T salary) {
    	this.salary=salary;
    }
    public int getId() {
    	return id;
    }
    public int getAge() {
    	return age;
    }
    public String getDept() {
    	return dept;
    }
    public T getSalary() {
    	return salary;
    }
    public void display() {
    	System.out.println("employ id"+this.getId());
    	System.out.println("employ age"+this.getAge());
    	System.out.println("employ dept"+this.getDept());
    	System.out.println("employ salary"+this.getSalary());
    }
    public static void sayHello()
    {
    	System.out.println("Hello employee\n");
    }
   /* @Override
   public void appraise<String>() {
    	System.out.println("Employ apparised");
    }*/
    public static void main(String[] args) {
		Employ<Integer> e=new Employ<Integer>();
		e.setSalary(10);
		System.out.println(e.getSalary());
		
    }
	
}
